package com.oleh.model;

import com.oleh.model.exc.BadNumberException;
import com.oleh.model.exc.SomeAutoClosebleClass;

import java.io.IOException;
import java.util.List;

public class Model implements ModelInt {
    SomeAutoClosebleClass autoClosebleClass;

    public Model() {


    }

    @Override
    public void setRange(int x, int y) throws Exception {
       try(SomeAutoClosebleClass someAutoClosebleClass = new SomeAutoClosebleClass(x, y);) {
           autoClosebleClass = someAutoClosebleClass;
       }
        autoClosebleClass.generateList();
    }

    @Override
    public List<Integer> buildSequence() {
        return autoClosebleClass.getIntegers();
    }
}
