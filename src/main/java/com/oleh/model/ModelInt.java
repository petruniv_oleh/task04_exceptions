package com.oleh.model;

import com.oleh.model.exc.BadNumberException;

import java.io.IOException;
import java.util.List;

public interface ModelInt {

    void setRange(int x, int y) throws Exception;
    List<Integer> buildSequence();

}
