package com.oleh.model.exc;

public class BadNumberException extends Exception {
    public BadNumberException(String message) {
        super(message);
    }
}
