package com.oleh.model.exc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SomeAutoClosebleClass implements AutoCloseable {
    private List<Integer> integers;
    private int x;
    private int y;

    public SomeAutoClosebleClass(int x, int y) {
        integers = new ArrayList<>();
        this.x = x;
        if (x < 2) {
            throw new WrongNumberException("you entered to low number");
        }
        if (x > 20) {
            throw new WrongNumberException("your number is too big");
        }
        this.y = y;
        if (y < 20) {
            throw new WrongNumberException("you entered to low number");
        }
        if (y > 100) {
            throw new WrongNumberException("your number is too big");
        }
    }


    public void generateList() throws BadNumberException {
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            if (i != 6) {
                integers.add(r.nextInt(y) + x);
            }
            else {
                integers.add(6);
            }
            if (integers.get(i) == 6 || integers.get(i) == 66) {
                throw new BadNumberException("you get " + integers.get(i) + " number");
            }
        }

    }

    public List<Integer> getIntegers() {
        return integers;
    }

    public void close() throws Exception {
        System.out.println("i`m closed");
        throw new Exception("new exc");
    }
}
