package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.Model;
import com.oleh.model.exc.BadNumberException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

public class View implements ViewInt {
    Controller controller;

    public View() {
        controller = new Controller();
        menu();
    }

    @Override
    public void menu() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("\n Menu: ");
        System.out.println("==================");
        System.out.println("1. Input values range");
        System.out.println("2. Get sequence");
        System.out.println("3. Exit");
        System.out.println("==================");
        System.out.println("Enter a value: ");
        switch (sc.nextInt()) {
            case 1:
                inputSequence();
                break;
            case 2:
                showSequence();
                break;
            case 3:
                Runtime.getRuntime().exit(0);
                break;
            default:
                break;
        }
    }

    @Override
    public void inputSequence() {
        Scanner sc = new Scanner(System.in, StandardCharsets.UTF_8);
        System.out.println("Enter first number (>2 and <20)");
        int firstNumber = sc.nextInt();
        System.out.println("Enter second number (>20 and <100)");
        int secondNumber = sc.nextInt();
        try {
            controller.setRange(firstNumber, secondNumber);
        } catch (BadNumberException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    @Override
    public void showSequence() {
        List<Integer> list = controller.getList();
        for (int x:list
             ) {
            System.out.print("\t"+x);

        }
        menu();
    }
}
