package com.oleh.controller;

import com.oleh.model.exc.BadNumberException;

import java.io.IOException;
import java.util.List;

public interface ControllerInt {
    void setRange(int first, int last) throws Exception;
    List<Integer> getList();

}
