package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.exc.BadNumberException;

import java.io.IOException;
import java.util.List;

public class Controller implements ControllerInt {

    Model model;

    public Controller() {
       model = new Model();
    }

    @Override
    public void setRange(int first, int last) throws Exception {
        model.setRange(first, last);
    }

    @Override
    public List<Integer> getList() {

        return model.buildSequence();
    }
}
